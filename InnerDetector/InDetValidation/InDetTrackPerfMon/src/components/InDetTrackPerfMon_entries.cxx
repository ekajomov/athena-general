/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "InDetTrackPerfMon/InDetTrackPerfMonTool.h"
#include "InDetTrackPerfMon/TrackAnalysisDefinitionSvc.h"
/// TODO - To be included in later MRs
//#include "InDetTrackPerfMon/TrackRoiSelectionTool.h"
//#include "InDetTrackPerfMon/RoiSelectionTool.h"
//#include "InDetTrackPerfMon/InDetGeneralSelectionTool.h"
//#include "InDetTrackPerfMon/InDetTrackObjectSelectionTool.h"
//#include "InDetTrackPerfMon/DeltaRtrackMatchingTool_trk.h"
//#include "InDetTrackPerfMon/DeltaRtrackMatchingTool_trkTruth.h"
//#include "InDetTrackPerfMon/TrackTruthMatchingTool.h"
//#include "InDetTrackPerfMon/HistogramDefinitionSvc.h"
//#include "InDetTrackPerfMon/ReadJsonHistoDefTool.h"
//#include "InDetTrackPerfMon/InDetTruthDecoratorAlg.h"
//#include "InDetTrackPerfMon/InDetOfflineElectronDecoratorAlg.h"
//#include "InDetTrackPerfMon/InDetOfflineMuonDecoratorAlg.h"
//#include "InDetTrackPerfMon/InDetOfflineTauDecoratorAlg.h"

DECLARE_COMPONENT( TrackAnalysisDefinitionSvc )
DECLARE_COMPONENT( InDetTrackPerfMonTool )
/// TODO - To be included in later MRs
//DECLARE_COMPONENT( ReadJsonHistoDefTool )
//DECLARE_COMPONENT( IDTPM::HistogramDefinitionSvc )
//DECLARE_COMPONENT( TrackRoiSelectionTool )
//DECLARE_COMPONENT( RoiSelectionTool )
//DECLARE_COMPONENT( IDTPM::InDetGeneralSelectionTool )
//DECLARE_COMPONENT( IDTPM::InDetTrackObjectSelectionTool )
//DECLARE_COMPONENT( IDTPM::DeltaRtrackMatchingTool_trk )
//DECLARE_COMPONENT( IDTPM::DeltaRtrackMatchingTool_trkTruth )
//DECLARE_COMPONENT( IDTPM::TrackTruthMatchingTool )
//DECLARE_COMPONENT( InDetTruthDecoratorAlg )
//DECLARE_COMPONENT( InDetOfflineElectronDecoratorAlg )
//DECLARE_COMPONENT( InDetOfflineMuonDecoratorAlg )
//DECLARE_COMPONENT( InDetOfflineTauDecoratorAlg )
